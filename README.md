# 2 x 30 W RMS Amplifier for Raspberry Pi

This 2 x 30 W RMS Amplifier for Raspberry Pi is an open source project based
on the [TAS5825M](http://www.ti.com/lit/ds/symlink/tas5825m.pdf) chip from
Texas Instrument. It takes the form of HAT, that back-powers the Pi.

While its output power is relatively limited, it sounds great and for me it's
a big change from my previous DAC, preamplifier and amplifier, all in 19"
enclosures, for basically the same performances. It's incredible how such
a small chip can output so much power with so good quality.

I am using it with a Raspberry Pi 3B+. I have installed
[PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) on it,
that way I can play audio remotely from various computers.


## Photo

[![2 x 30 W RMS amplifier on top of a Raspberry Pi 3B+](images/rpi-amp-small.jpeg)](images/rpi-amp.jpeg)


## Hardware

The board has been designed using KiCad. The [schematic](images/schematic.png)
is inspired from the TAS5825M evaluation module. It is used with a
10µH / 680nF output filter, using the same Murata inductors as in the EVM. The
PVDD power supply is bypassed with 2 x 680 µF + 2 x 10 µF ceramic + 100 nF
ceramic capacitors per channel. There is also a 5V 3A DC/DC converter based on
a LMR33630 to back-power the Raspberry Pi from the 24 V power supply, and a
LTC4365 based DC input protection to protect the board from wrong polarity,
under-voltage or over-voltage. The board accepts voltages between 20 and
28 V. The 3 GPIO outputs from the TAS5825M are connected to 3 red LEDs,
that might be configured to display various information (fault, warning,
auto-mute, etc.). Finally the board also contains the 24LC32A EEPROM required
for Raspberry Pi Hats.

The PCB is a simple two layers PCB that follows the
[Raspberry Pi Hat requirements](https://github.com/raspberrypi/hats), although
it doesn't have the optional display flex cutout, the optional camera flex
slot, nor the 4-pin PoE header.

All the components from the [bill of material](BOM.txt) should be easily
available from distributors that also sell the TAS5825M. They might be
difficult to solder for people not used to SMD, especially for the LMR33630
that has an exposed pad and for the TAS5825M that is using a QFN32 package. It
is possible to solder both using a hot air gun, I have personally not used
a stencil.

The board should be powered by a 24V / 90 W switching power supply power
adaptor.


## Software

The software part is not yet finalized. I have written a quick and dirty
Linux kernel module to present the TAS5825M chip as a simple audio card.
The driver doesn't support any of the advanced features of that chip (volume
control, audio processor, etc). It is also not good quality enough to be
upstreamable. It has been sufficient for my usage, so I didn't take the time
to improve it.

The Linux kernel module should build fine on a system with a recent kernel
(so far I only tested 5.4 and 5.5) and with the kernel headers available. It
could be built and installed with the following command:

```
  cd tas5825m-module
  make
  make modules_install
```

The tricky part is to modify the device tree to get the board recognized by
the kernel. In the long term the 24L32 EEPROM should be used for that, but I
haven't look at how it works yet. The patch I use on my system is
[available](tas5825m-module/dts.patch), it should probably be adapted
depending on the system and the kernel version. I used the `dtc` tool to dump
the original device tree binary file into a source format, and later to
rebuild the binary version. It should be easier to use device tree overlays
but again I haven't looked at that yet.


## Sources

All the sources of the repository are available
[here](https://git.aurel32.net/electronics/rpi-amp.git).


## License

The contents of this repository, excluding the `tas5825m-module` directory, is
released under the [Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0)](LICENSE).
