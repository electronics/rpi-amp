// SPDX-License-Identifier: GPL-2.0
/*
 * tas5825.h - ALSA SoC Texas Instruments TAS5825M Audio Amplifier
 *
 * Copyright (C) 2020 Aurelien Jarno <aurelien@aurel32.net>
 *
 * Author: Aurelien Jarno <aurelien@aurel32.net>
 */

#ifndef __TAS5825M_H__
#define __TAS5825M_H__

#define TAS5825M_RATES (SNDRV_PCM_RATE_48000 | \
			SNDRV_PCM_RATE_96000 | \
			SNDRV_PCM_RATE_192000)
#define TAS5825M_FORMATS (SNDRV_PCM_FMTBIT_S16_LE | \
			  SNDRV_PCM_FMTBIT_S24_LE | \
			  SNDRV_PCM_FMTBIT_S32_LE)

/* Register Address Map */
#define TAS5825M_RESET_CTRL		0x00

#endif /* __TAS5825M_H__ */

